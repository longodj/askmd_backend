# -*- coding: utf-8 -*-
"""Example Google style docstrings.

This module demonstrates documentation as specified by the `Google Python
Style Guide`_. Docstrings may extend over multiple lines. Sections are created
with a section header and a colon followed by a block of indented text.

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python example_google.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    module_level_variable1 (int): Module level variables may be documented in
        either the ``Attributes`` section of the module docstring, or in an
        inline docstring immediately following the variable.

        Either form is acceptable, but the two should not be mixed. Choose
        one convention to document module level variables and be consistent
        with it.

Todo:
    * For module TODOs
    * You have to also use ``sphinx.ext.todo`` extension

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html

"""
from ftplib import FTP
from io import StringIO, BytesIO
import jsonpickle
import logging
import requests
import xml.etree.ElementTree as ET

from multiprocessing.dummy import Pool as ThreadPool

from askmd_backend.crawler.article import Article
from askmd_backend.crawler.journal import Journal

__author__ = "David Longo"
__copyright__ = "David Longo"
__license__ = "gpl3"

_logger = logging.getLogger(__name__)

abstracts = []

class PMC(Journal):
    """The summary line for a class docstring should fit on one line.

    If the class has public attributes, they may be documented here
    in an ``Attributes`` section and follow the same formatting as a
    function's ``Args`` section. Alternatively, attributes may be documented
    inline with the attribute's declaration (see __init__ method below).

    Properties created with the ``@property`` decorator should be documented
    in the property's getter method.

    Attributes:
        attr1 (str): Description of `attr1`.
        attr2 (:obj:`int`, optional): Description of `attr2`.

    """
    URI = "ftp.ncbi.nlm.nih.gov"
    LIST_URI = "/pub/pmc/oa_non_comm_use_pdf.csv"

    def __init__(self):
        """
        Initialize Journal with PMC endpoint.

        Initialize Journal with PMC endpoint.

        Args:
            None

        Returns:
            None

        Raises:
            None

        """
        super().__init__()

    def read_article(accession: str, connPool):
        """
        Read article from PMC journal using Open Access Interface.

        Reads an article from PMC journal using Open Access Interface by
        accession number. Returns markup of artcile.

        Args:
            accession (str): Accession identifier of article

        Returns:
            Article: Article object initialized with markup

        Raises:
            Exception: None

        """
        article_URI = "https://www.ncbi.nlm.nih.gov/pmc/oai/oai.cgi?" \
                      "verb=GetRecord&identifier=" \
                      "oai:pubmedcentral.nih.gov:" + \
                      accession[3:] + "&metadataPrefix=pmc"
        _logger.info("\nAccessing article at URI: {}\n".format(article_URI))
        if connPool is None:
            r = requests.get(article_URI, allow_redirects=True)
        else:
            r = connPool.get(article_URI, allow_redirects=True)
        _logger.debug("\nSTATUS CODE: {}\n".format(r.status_code))
        if r.status_code == requests.codes.ok:
            markup = ET.fromstring(r.text)
            _logger.debug("\nReceived: {}\n".format(markup))
            #_logger.debug(ET.tostring(markup))
            return Article(markup, PMC)
        else:
            return None

    @staticmethod
    def parse_markup(cls):
        """
        Markup handler specific to PMC for Article.

        PMC specific handler for Article markup

        Args:
            cls (Article): Article with markup to parse.
                - Mutable, will set instance variables.

        Returns:
            None

        Raises:
            None

        """
        ns = {
            "OAI-PMH": "http://www.openarchives.org/OAI/2.0/"#,
            #"article": "https://dtd.nlm.nih.gov/ns/archiving/2.3/"
        }

        ''' Get response information '''
        cls._lastRetrieved = \
            cls._markup.find("./OAI-PMH:responseDate", ns)
        if cls._lastRetrieved is not None:
            cls._lastRetrieved = cls._lastRetrieved.text

        _logger.debug("\nLast Retrieved: {}\n".format(cls._lastRetrieved))

        ''' Get header information '''
        cls._date = \
            cls._markup.find(".//OAI-PMH:datestamp", ns)
        if cls._date is not None:
            cls._date = cls._date.text
        _logger.debug("Date Stamp: {}\n".format(cls._date))

        ns["article"] = cls._markup.find('.//OAI-PMH:metadata',ns)[0].attrib['{http://www.w3.org/2001/XMLSchema-instance}schemaLocation'].split()[0]

        cls._title = \
            cls._markup.find(".//article:article-title", ns)
        if cls._title is not None:
            cls._title = cls._title.text
        _logger.debug("Article Title: {}\n".format(cls._title))

        abstractElem = \
            cls._markup.find(".//article:abstract", ns)

        if abstractElem is None:
            _logger.debug("Abstract not found")
            return

        cls._abstract = ""
        for section in abstractElem:
            if "sec" in section.tag:
                for child in section:
                    if "title" not in child.tag:
                        #_logger.debug("\n\tChild: {}".format(child.text))
                        cls._abstract += \
                            u' '.join(child.itertext())
            else:
                if "title" not in section.tag:
                    #_logger.debug("\n\tChild: {}".format(child.text))
                    cls._abstract += \
                        u' '.join(section.itertext())
        _logger.debug("Abstract: {}\n".format(cls._abstract))

    @staticmethod
    def read_abstracts(n: int, s: int, connPool, cache):
        ftp = FTP(PMC.URI)
        ftp.login()
        r = BytesIO()
        ftp.retrbinary("RETR /pub/pmc/oa_non_comm_use_pdf.csv", r.write)
        articleData = StringIO(r.getvalue().decode('UTF-8'))
        #print("Reading {} articles:".format(n))
        #print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
        if n is None:
            print("NO N PROVIDED")
            articleList = articleData.read().split('\n')[1:]
        else:
            articleList = []
            headerLine = articleData.readline()
            for j in range(0,s):
                headerLine = articleData.readline()
            for i in range(0, n):
                #print(i)
                articleList.append(articleData.readline())

        total = len(articleList)
        def articleThread(article, cache, connPool, abstracts):
            accession = article.split(',')[2]
            try:
                if accession not in cache:
                    readArticle = PMC.read_article(accession, connPool)
                    abstracts.append(readArticle)
                    cache[accession] = jsonpickle.encode(readArticle)
                else:
                    abstracts.append(jsonpickle.decode(cache[accession]))
            except:
                pass
            _logger.debug("\t* Article read: {}".format(accession))

        pool = ThreadPool(16)
        pool.map(lambda a: articleThread(a, cache, connPool, abstracts), articleList)
        pool.close()
        pool.join()

        return abstracts
