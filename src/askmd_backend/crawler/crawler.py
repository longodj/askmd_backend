#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
This is a skeleton file that can serve as a starting point for a Python
console script. To run this script uncomment the following lines in the
[options.entry_points] section in setup.cfg:

    console_scripts =
         fibonacci = askmd_backend.skeleton:run

Then run `python setup.py install` which will install the command `fibonacci`
inside your current environment.
Besides console scripts, the header (i.e. until _logger...) of this file can
also be used as template for Python modules.

Note: This skeleton file can be safely removed if not needed!
'''

import argparse
import json
import logging
import requests
#from requests.adapters import HTTPAdapter
import urllib3
import sys
import time
import traceback
import random
from datetime import datetime
from multiprocessing.dummy import Pool as ThreadPool

from xml.etree import ElementTree as ET

from askmd_backend import __version__
from askmd_backend.crawler.journal import Journal
from askmd_backend.crawler.pmc import PMC
from askmd_backend.extractor.extractor import Extractor

__author__ = "David Longo"
__copyright__ = "David Longo"
__license__ = "gpl3"

_logger = logging.getLogger(__name__)
http = urllib3.PoolManager(maxsize=16, block=True)

SEMREP_URI = [  "http://54.144.252.47:5000",
                "http://54.209.148.80:5000",
                "http://52.73.214.242:5000" ]
#SEMREP_URI = "http://127.0.0.1:5000"
#SEMREP_URI = "http://34.201.138.66:5000"

class Crawler:
    def __init__(self, journal):
        if journal == "PMC":
            self._journal = PMC()
            self._journalType = PMC
        else:
            raise ValueError("Journal not supported.")

    def get_journal(self):
        return self._journal


def parse_args(args):
    """Parse command line parameters

    Args:
      args ([str]): command line parameters as list of strings

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace

    """
    parser = argparse.ArgumentParser(
        description="Crawl journals for articles")
    parser.add_argument(
        '--version',
        action='version',
        version='askmd_backend {ver}'.format(ver=__version__))
    parser.add_argument(
        '-v',
        '--verbose',
        dest="loglevel",
        help="set loglevel to INFO",
        action='store_const',
        const=logging.INFO)
    parser.add_argument(
        '-vv',
        '--very-verbose',
        dest="loglevel",
        help="set loglevel to DEBUG",
        action='store_const',
        const=logging.DEBUG)
    parser.add_argument(
        '-j',
        '--journal',
        dest="journal",
        help="Choose which journal to crawl",
        type=str,
        default="PMC")
    parser.add_argument(
        '-a',
        '--abstract',
        dest="accession",
        type=str
    )
    parser.add_argument(
        '-n',
        '--num-articles',
        dest="numArticles",
        type=int,
        default=0
    )
    parser.add_argument(
        '-s',
        '--start-index',
        dest="startIndex",
        type=int,
        default=0
    )
    parser.add_argument(
        '-e',
        '--extract',
        dest='extract',
        action='store_true'
    )
    parser.add_argument(
        '-c',
        '--cache',
        dest='cache',
        action='store_true'
    )
    return parser.parse_args(args)


def setup_logging(loglevel):
    """Setup basic logging.

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    #logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s:%(thread)d:"
    logformat = "[%(asctime)s]:%(thread)d:%(message)s"
    logging.basicConfig(level=loglevel, stream=sys.stdout,
                        format=logformat, datefmt="%Y-%m-%d %H:%M:%S")


def main(args):
    """Main entry point all owing external calls

    Args:
      args ([str]): command line parameter list
    """
    args = parse_args(args)
    setup_logging(args.loglevel)
    _logger.debug("Starting crazy calculations...")
    print("Crawling {} as per args.".format(args.journal))
    _logger.info("Script ends here")

    crawler = Crawler(args.journal)

    if args.cache is True:
        try:
            with open('cache.json') as json_file:
                cache = json.load(json_file)
        except:
            cache = {}
    else:
        cache = {}

    corpus = []
    s = requests.Session()
    #s.mount('http://', HTTPAdapter(pool_connections=10, pool_maxsize=10))
    #s.mount('https://', HTTPAdapter(pool_connections=10, pool_maxsize=10))
    #s.keep_alive = False
    if args.accession != None:
        article = crawler._journalType.read_article(args.accession)
        abstract = article.get_abstract()

        corpus.append(
            (Journal.Location.LOCAL, abstract)
        )

        _logger.debug("Added the following to corpus:")
        _logger.debug(abstract)
    else:
        if args.numArticles is not None:
            if args.numArticles != -1:
                for i,abstract in enumerate(crawler._journalType.read_abstracts(args.numArticles,
args.startIndex, s, cache)):
                    corpus.append(
                        (Journal.Location.LOCAL, abstract)
                    )
                    _logger.debug("Added {} to corpus:".format(i))
                    #_logger.debug(abstract)
        else:
            print("Journal Information Here!")

    if args.cache is True:
        with open('cache.json', 'w') as outfile:
            json.dump(cache, outfile)

    headers = {
        'Content-type': 'application/json',
    }
    successes = 0
    threads = []
    pool = ThreadPool(32)
    def uploadArticle(article):
        try:
            data = {'article': str(article[1])}
            encoded_data = json.dumps(data).encode('utf-8')
            while True:
                try:
                    random.seed(datetime.now())
                    rruri = SEMREP_URI[random.randint(0,2)]
                    r = http.request('POST', rruri + "/askmd/api/v1.0/semrep", body=encoded_data,headers=headers, timeout=30)
                    break
                except:
                    time.sleep(30)
            if r is None:
                return
            #requests.session().close()

            _logger.debug("Response from SemRep Server:")
            _logger.debug("\t *{}".format(r.status))
            #_logger.debug(r.text)

            #print(r.data)
            #print(type(r.data))
            x = ET.fromstring(json.loads(r.data.decode('utf-8'))['semrep'])
            Extractor.extract(x)
            del x
        except Exception as e:
            print(str(e))
            traceback.print_tb(e.__traceback__)
        #print("Successes: {}".format(successes))

    #for article in corpus:
        #_logger.debug("Response from SemRep Server:")
        #_logger.debug(x)
    pool.map(lambda a: uploadArticle(a), corpus)
    pool.close()
    pool.join()
        

def crawler_run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    crawler_run()
