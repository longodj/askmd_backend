import json
import requests

from xml.etree import ElementTree as ET

article = "Patients with abnormal breast findings ( n  = 413) were examined by mammography, sonography and magnetic resonance (MR) mammography; 185 invasive cancers, 38 carcinoma  in situ  and 254 benign tumours were confirmed histologically. Sensitivity for mammography was 83.7%, for sonography it was 89.1% and for MR mammography it was 94.6% for invasive cancers. In 42 patients with multifocal invasive cancers, multifocality had been detected by mammography and sonography in 26.2%, and by MR mammography in 66.7%. In nine patients with multicentric cancers, detection rates were 55.5, 55.5 and 88.8%, respectively. Carcinoma  in situ  was diagnosed by mammography in 78.9% and by MR mammography in 68.4% of patients. Combination of all three diagnostic methods lead to the best results for detection of invasive cancer and multifocal disease. However, sensitivity of mammography and sonography combined was identical to that of MR mammography (ie 94.6%)."

headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
r = requests.post("http://127.0.0.1:5000/askmd/api/v1.0/semrep", data=json.dumps({'article': article}), headers=headers)

x = ET.fromstring(json.loads(r.text)['semrep'])

print(ET.tostring(x))
