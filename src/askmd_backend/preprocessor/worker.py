#!flask/bin/python
import subprocess

from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route('/')
def index():
    return "Hello, World!"

@app.route('/askmd/api/v1.0/semrep', methods=['POST'])
def semrep():
    print("Handling request: ")
    print(request.args)
    if not request.json or not 'article' in request.json:
        return jsonify({'error':'article not sent'}, 400)
    article = request.json['article'].encode('utf-8')
    cmd = '/home/ec2-user/NIH/public_semrep/bin/semrep.v1.8 -L 2018 -Z 2018AA -y -A -M -X'
    try:
        process = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, close_fds=True)
        process.stdin.write(article)
        response = process.communicate()[0].decode('utf-8')
        article = response[response.find("<?xml"):response.find("</SemRepAnnotation>")+19]
        process.stdin.close()
        process.kill()
    except:
        article = ''
        pass
    return jsonify({'semrep': article}),200

if __name__ == '__main__':
    app.run(host= '0.0.0.0', debug=False, threaded=True)
