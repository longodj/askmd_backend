#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This is a skeleton file that can serve as a starting point for a Python
console script. To run this script uncomment the following lines in the
[options.entry_points] section in setup.cfg:

    console_scripts =
         fibonacci = askmd_backend.skeleton:run

Then run `python setup.py install` which will install the command `fibonacci`
inside your current environment.
Besides console scripts, the header (i.e. until _logger...) of this file can
also be used as template for Python modules.

Note: This skeleton file can be safely removed if not needed!
"""

from __future__ import print_function  # Python 2/3 compatibility

import argparse
import logging
import sys
from xml.etree import ElementTree as ET

from gremlin_python import statics
from gremlin_python.driver.driver_remote_connection import (
    DriverRemoteConnection
)
from gremlin_python.process.graph_traversal import __
from gremlin_python.process.strategies import *
from gremlin_python.process.traversal import Operator, P, T
from gremlin_python.structure.graph import Graph

from askmd_backend import __version__

__author__ = "David Longo"
__copyright__ = "David Longo"
__license__ = "gpl3"

_logger = logging.getLogger(__name__)

#f = open("pmc13900.xml", "r")
#_pmc13900 = f.read()
#f.close()
'''
graph = Graph()

g = graph.traversal().withRemote(
    DriverRemoteConnection(
    #    'ws://localhost:8182/gremlin',
        'ws://askmd.cmhsmb44ld57.us-east-1.neptune.amazonaws.com:8182/gremlin',
        'g'
    )
)
'''
counter = 0


class Extractor:
    """
    Extract features from corpus.

    Feature extractor for medical text.

    """

    def __init__(self, text):
        """
        Initialize extractor.

        A bit longer description.

        Args:
            text (str): description

        Returns:
            type: description

        Raises:
            Exception: description

        """
        self._text = text
        print("Extracting")
        print(self._text)

    @staticmethod
    def extract(xml:ET.Element):
        entities = {}
        predications = []
        for u in xml.findall(".//Utterance"):
            sentence = (u.get("id"),u.get("text"))
            for e in u:
                if "Entity" in e.tag:
                    entities[e.get("id")] = {
                        "cui"       : e.get("cui"),
                        "name"      : e.get("name"),
                        "semtype"  : e.get("semtypes"),
                        "text"      : e.get("text"),
                        "score"     : e.get("score"),
                        "begin"     : e.get("begin"),
                        "end"       : e.get("end")
                    }
                if "Predication" in e.tag:
                    for p in e:
                        if "Subject" in p.tag:
                            subject = entities[p.get("entityID")]["cui"]
                        if "Predicate" in p.tag:
                            link = (p.get("indicatorType"),p.get("type"))
                        if "Object" in p.tag:
                            object = entities[p.get("entityID")]["cui"]
                    predications.append((subject,link,object,sentence))

        neptune = DriverRemoteConnection(
        #    'ws://localhost:8182/gremlin',
            'ws://askmd.cmhsmb44ld57.us-east-1.neptune.amazonaws.com:8182/gremlin',
            'g'
        )
        graph = Graph()
        g = graph.traversal().withRemote(
            neptune
        )
        #print(g)
        #print(g.V().count().next())
        #g.V().drop().iterate()
        #g.E().drop().iterate()
        #print(g.V().toList())

        #print(g.V().count().next())
        #print(g.V("C0425770").outE("PREP").count().next())
        try:
            for k,v in entities.items():
                #print("{}: {} | {} - {}".format(
                #    k,
                #    v['name'],
                #    v['semtype'],
                #    v['text']
                #))

                if v['name'] == None:
                    continue

                if not g.V(v['cui']).toList():
                    #print("\tAdding vertex {}".format(v['name']))
                    g.addV(v['cui']).property(
                            T.id,       v["cui"]
                        ).property(
                            "cui",      v["cui"]
                        ).property(
                            "semtype",  v["semtype"]
                        ).property(
                            "name",     v["name"]
                        ).property(
                            "score",    v["score"]
                        ).next()
            successes = 0
            for p in predications:
                subject = g.V(p[0])
                #print(type(subject))
                #print("\t{}".format(subject))
                obj = g.V(p[2])
                #print("\t{}".format(obj))
                #if subject != None and object != None:
                    #if not g.V(p[0]).out(p[1]).hasId(p[2]).hasNext():
                try:
                    if subject is not None and obj is not None and p[1] is not None and p[3] is not None:
                        e = subject.addE(p[1][1]).to(obj).property("type",p[1][0]).property("sentence",p[3]).next()
                        #print("{} -> {} -> {}".format(
                        #    p[0],
                        #    p[1],
                        #    p[2]
                        #))
                    successes += 1
                except:
                    _logger.error("EXCEPTION adding to graph")
                #print(e)
            _logger.info("Successfully added {} predications".format(successes)) 
            #print("Deleting graph")
            #del g
            #del graph
            #print(extraction)
        except Exception as e:
            print(str(e))
            traceback.print_tb(e.__traceback__)
        neptune.close()

def parse_args(args):
    """Parse command line parameters

    Args:
      args ([str]): command line parameters as list of strings

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace

    """
    parser = argparse.ArgumentParser(
        description="Crawl journals for articles")
    parser.add_argument(
        '--version',
        action='version',
        version='askmd_backend {ver}'.format(ver=__version__))
    parser.add_argument(
        '-v',
        '--verbose',
        dest="loglevel",
        help="set loglevel to INFO",
        action='store_const',
        const=logging.INFO)
    parser.add_argument(
        '-vv',
        '--very-verbose',
        dest="loglevel",
        help="set loglevel to DEBUG",
        action='store_const',
        const=logging.DEBUG)
    parser.add_argument(
        '-f',
        '--file',
        dest="file",
        help="Provide a file to read",
        type=str)
    parser.add_argument(
        '-a',
        '--abstract',
        dest="accession",
        type=str
    )
    return parser.parse_args(args)


def setup_logging(loglevel):
    """Setup basic logging.

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(level=loglevel, stream=sys.stdout,
                        format=logformat, datefmt="%Y-%m-%d %H:%M:%S")


def main(args):
    """Main entry point allowing external calls

    Args:
      args ([str]): command line parameter list
    """
    args = parse_args(args)
    setup_logging(args.loglevel)
    _logger.debug("Starting crazy calculations...")
    _logger.info("Script ends here")

    #if args.file is not None:
    #    f = open(args.file, 'r')
    #    text = f.read()
    #    f.close()
    #else:
    #    text = sys.stdin.read()

    if args.accession == "PMC13900":
        extraction = _pmc13900
        xml = ET.fromstring(extraction)
        Extractor.extract(xml)

    #extractor = Extractor(text)


def extractor_run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    extractor_run()
