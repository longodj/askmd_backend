from gremlin_python import statics
from gremlin_python.driver.driver_remote_connection import (
    DriverRemoteConnection
)
from gremlin_python.process.graph_traversal import __
from gremlin_python.process.strategies import *
from gremlin_python.process.traversal import Operator, P, T
from gremlin_python.structure.graph import Graph

graph = Graph()

g = graph.traversal().withRemote(
    DriverRemoteConnection(
#        'ws://localhost:8182/gremlin',
        'ws://askmd.cmhsmb44ld57.us-east-1.neptune.amazonaws.com:8182/gremlin',
        'g'
    )
)

print(g.V().count().next())
g.V().drop().iterate()
g.E().drop().iterate()
print(g.V().count().next())
