# -*- coding: utf-8 -*-
"""Example Google style docstrings.

This module demonstrates documentation as specified by the `Google Python
Style Guide`_. Docstrings may extend over multiple lines. Sections are created
with a section header and a colon followed by a block of indented text.

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python example_google.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    module_level_variable1 (int): Module level variables may be documented in
        either the ``Attributes`` section of the module docstring, or in an
        inline docstring immediately following the variable.

        Either form is acceptable, but the two should not be mixed. Choose
        one convention to document module level variables and be consistent
        with it.

Todo:
    * For module TODOs
    * You have to also use ``sphinx.ext.todo`` extension

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html

"""
''' Standard Imports '''
import logging
import xml.etree.ElementTree as ET

import boto3
import requests

from askmd_backend.extractor.graph import Graph

''' Third Party Imports'''

''' Project Imports '''

__author__ = "David Longo"
__copyright__ = "David Longo"
__license__ = "gpl3"

_logger = logging.getLogger(__name__)


class Neptune(Graph):
    """The summary line for a class docstring should fit on one line.

    If the class has public attributes, they may be documented here
    in an ``Attributes`` section and follow the same formatting as a
    function's ``Args`` section. Alternatively, attributes may be documented
    inline with the attribute's declaration (see __init__ method below).

    Properties created with the ``@property`` decorator should be documented
    in the property's getter method.

    Attributes:
        attr1 (str): Description of `attr1`.
        attr2 (:obj:`int`, optional): Description of `attr2`.

    """

    def __init__(self):
        """
        Initialize Graph with Neptune Specifics.

        Initialize Graph with Neptune Specifics.

        Args:
            None

        Returns:
            None

        Raises:
            None

        """
        super().__init__()
        self._client = boto3.client('neptune')
    def read_article(accession: str):
        """
        Read article from PMC journal using Open Access Interface.

        Reads an article from PMC journal using Open Access Interface by
        accession number. Returns markup of artcile.

        Args:
            accession (str): Accession identifier of article

        Returns:
            Article: Article object initialized with markup

        Raises:
            Exception: None

        """
        article_URI = "https://www.ncbi.nlm.nih.gov/pmc/oai/oai.cgi?" \
                      "verb=GetRecord&identifier=" \
                      "oai:pubmedcentral.nih.gov:" + \
                      accession[3:] + "&metadataPrefix=pmc"
        _logger.debug("Accessing article at URI: {}".format(article_URI))
        print("Accessing article at URI: {}".format(article_URI))
        r = requests.get(article_URI, allow_redirects=True)
        if r.status_code == requests.codes.ok:
            markup = ET.fromstring(r.text)
            return Article(markup, PMC)
        else:
            return None
