#!/usr/bin/env python
# -*- coding: utf-8 -*-

import hashlib
from xml.etree import ElementTree as ET

from askmd_backend.crawler.pmc import PMC

# import pytest


__author__ = "David Longo"
__copyright__ = "David Longo"
__license__ = "gpl3"


def test_pmc():
    '''
    assert fib(1) == 1
    assert fib(2) == 1
    assert fib(7) == 13
    with pytest.raises(AssertionError):
        fib(-10)
    '''
    article = PMC.read_article("PMC156895")

    ns = {"OAI-PMH": "http://www.openarchives.org/OAI/2.0/"}

    et = article._markup.find("OAI-PMH:GetRecord", ns)
    articlestr = ET.tostring(et, encoding='utf8', method='xml')

    mdigest = hashlib.md5(articlestr).hexdigest()
    assert mdigest == "85ad09313b84c2b4ec422731a5c9b91e"
