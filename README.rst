=============
askmd_backend
=============


Automated biomedical knowledge graph construction and analysis


Description
===========

Requires SemRep v1.8.

Setup a virtual environment using virtualenv and install all requirements with pip.

To install run:
python setup.py install

On Worker:
Run MetaMap servers:
./bin/wsdserverctl start
./bin/skrmedpostctl start

To run the Flask app in src/askmd_backend/preprocessor/, run:
gunicorn -w [number of workers] -b 0.0.0.0:5000 worker:app

To run the crawler
seq [start_mult end_mult] | xargs -P [number of processes] -n 1 timeout [time]s askmd_crawl -j PMC -n[num_articles_at_once] -v -s

For example:
seq 1000 2000 | xargs -P 8 -n 1 timeout 600s askmd_crawl -j PMC -n100 -v -s

Note
====

This project has been set up using PyScaffold 3.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.
